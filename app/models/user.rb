class User < ActiveRecord::Base

  has_many :attachments

  validates :email, uniqueness: true
  validates_format_of :email, :with => /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/
  validates_length_of :password, minimum: 8

  def self.authenticate email, password
    user = find_by_email(email)
    if user && user.password == password
      user
    else
      nil
    end
  end
end
