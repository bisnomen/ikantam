require "application_responder"

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

  def signed_in?
    redirect_to new_session_path unless current_user
  end

  def current_user
    @current_user ||= User.find(cookies[:user_id]) if cookies[:user_id]
  rescue
    cookies.delete(:user_id)
    redirect_to root_path
  end

end
