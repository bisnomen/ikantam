class AttachmentsController < ApplicationController

  before_filter :signed_in?

  def index
    @attachments = Attachment.all.order(created_at: :desc)
    @attachment = Attachment.new
  end

  def create
    if params[:attachment]
      @attachment = Attachment.create(attachment_params)
      respond_with(@attachment, location: root_path)
    else
      redirect_to root_path
    end
  end

  private

  def attachment_params
    params.require(:attachment).permit(:file).merge(user_id: current_user.id)
  end
end
