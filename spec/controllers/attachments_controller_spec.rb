require 'rails_helper'

RSpec.describe AttachmentsController, type: :controller do

  let(:user) { create(:user) }

  before do 
    cookies[:user_id] = user.id
  end

  describe "GET #index" do 
    let!(:attachments) { create_list(:attachment, 2) }

    before do 
      get :index
    end

    it "return status 200" do 
      expect(response.status).to eq 200
    end

    it "render index view" do 
      expect(response).to render_template(:index)
    end

    it 'assign attachements' do
      expect(assigns(:attachments)).to match_array(attachments)
    end
  end

  describe "POST #create" do 
    it 'create new attachemtn' do 
      file = File.open(File.join(Rails.root, 'spec/support/shared/rails.png'))
      expect { post :create, attachment: { file: file } }.to change(Attachment, :count).by(1)
    end

    it 'redirect to root_path' do 
      post :create
      expect(response).to redirect_to root_path
    end
  end

end
