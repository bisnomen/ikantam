require 'rails_helper'

RSpec.describe SessionsController, type: :controller do

  describe 'GET #new' do 
    before do 
      get :new
    end

    it "return status 200" do 
      expect(response.status).to eq 200
    end

    it "render new view" do 
      expect(response).to render_template(:new)
    end
  end


  describe "POST #create" do 
    context'with valid attributes' do
      let!(:user) { create(:user) }
      it 'set cookie id' do
        post :create, email: user.email, password: user.password
        expect(cookies[:user_id]).to eq user.id
      end
    end

    context 'with invalid attributes' do
      it 'does not set user_id' do
        post :create, email: "some@email.com", password: "some_password"
        expect(cookies[:user_id]).to eq nil
        expect(response).to render_template(:new)
      end
    end
  end

end
