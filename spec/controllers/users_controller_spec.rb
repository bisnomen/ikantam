require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  describe 'GET #new' do 

    before do 
      get :new
    end

    it "return status 200" do 
      expect(response.status).to eq 200
    end

    it "render new view" do 
      expect(response).to render_template(:new)
    end
  end


  describe "POST #create" do 
    context'with valid attributes' do
      it 'save the new user in the database' do
        expect { post :create, user: attributes_for(:user) }.to change(User, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      it 'does not save the user' do
        expect { post :create, user: attributes_for(:user, email: "email") }.to_not change(User, :count)
      end
    end
  end

end
