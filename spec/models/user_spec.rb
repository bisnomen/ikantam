require 'rails_helper'

RSpec.describe User, type: :model do
  it { should allow_value("email@email.com").for(:email) }
  it { should_not allow_value("email").for(:email) }
  it { should validate_uniqueness_of :email }
  it { should validate_length_of(:password).is_at_least(8) }
  it { should have_many :attachments }

  describe 'validate user' do 
    context 'valid user' do 
      let!(:user) { create(:user) }
      it 'return user' do 
        expect(User.authenticate(user.email, user.password)).to eq user
      end
    end

    context 'invalid user' do 
      it 'return nil' do 
        expect(User.authenticate('some@email.com', 'some_password')).to eq nil
      end
    end
  end
end
